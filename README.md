Grouper
=======

Grouper is an authorization library that allows PHP applications to authorize a user given a specific Grouper group.

Please see the Grouper website for more information:

http://www.internet2.edu/grouper/
