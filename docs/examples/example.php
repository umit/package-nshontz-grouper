<?php

print("<pre>");

include('../../../../autoload.php');
include('config.php');

$grouper = new Grouper($host, $port, $path, $user, $password);

print("Is Auth Failure: ");
var_dump($grouper->is_user_authorized($example_user, $example_bad_group));
print("<br/><br/>");
print("Is Auth Success: ");
var_dump($grouper->is_user_authorized($example_user, $example_group));
print("<br/><br/>");
print("User Groups: ");
print_r($grouper->get_user_groups($example_user));
print("<br/><br/>");
print("Group Members: ");
print_r($grouper->get_group_members($example_group));