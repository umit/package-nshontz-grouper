<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grouper
 *
 * @author ns159438e
 */
class Grouper {

    private $user;
    private $password;
    private $uri;

    function __construct($host, $port, $path, $user, $password) {
        $this->user = $user;
        $this->password = $password;
        $this->uri = $host . ":" . $port . "/" . trim($path, "/") . "/";
    }

    /**
     * Get all the members for a given group
     * @param string $group
     * @return array 
     */
    function get_group_members($group) {
        $file = $this->uri . "groups/" . urlencode($group) . "/members";
        $group_members = array();
        $response = Httpful\Request::get($file)->expectsXml()->timeout(1)->authenticateWith($this->user, $this->password)->send();
        if ($response != null && $response->body->resultMetadata->success != "F") {
            $members = (array) $response->body->wsSubjects;
            foreach ($members['WsSubject'] as $member) {
                $group_members[] = (string) $member->id;
            }
        } else {
            error_log('debug - could not query grouper: ' . $this->user . '@' . $file);
        }

        return $group_members;
    }

    /**
     * Gets the groups for a specific user
     * @param string $user
     * @return array
     */
    function get_user_groups($user) {
        $file = $this->uri . 'subjects/' . $user . "/groups";
        $user_groups = array();
        $response = Httpful\Request::get($file)->expectsXml()->timeout(1)->authenticateWith($this->user, $this->password)->send();

	    if (!is_object($response->body)) {
            $response->body = simplexml_load_string($response->body);
        }
        if ($response != null && $response->body->resultMetadata->success != "F") {
            $groups = (array) $response->body->wsGroups;

	        if (count($groups) > 0) {
		        if(!is_array($groups['WsGroup'])) {
			        $groups['WsGroup'] = array($groups['WsGroup']);
		        }
                foreach ($groups['WsGroup'] as $group) {
                    $user_groups[] = (string) $group->name;
                }
            }
        } else {
            error_log('debug - could not query grouper: ' . $this->user . '@' . $file);
        }

        return $user_groups;
    }

    /**
     * verifies that given user is in the given gruoper group
     * @param string $user
     * @param string $auth_group
     * @return boolean 
     */
    function is_user_authorized($user, $auth_group) {
        $is_auth = false;
        $groups = $this->get_user_groups($user);
        if (in_array($auth_group, $groups)) {
            $is_auth = true;
        }
        return $is_auth;
    }

}