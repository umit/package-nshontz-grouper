<?php

/**

 *
 * @author Nick Shontz <nick.shontz@umontana.edu>
 */
require(dirname(dirname(dirname(__FILE__))) . '/src/Grouper.php');
require('httpful-0.2.0.phar');

class GrouperTest extends \PHPUnit_Framework_TestCase {

    function __construct() {
        parent::__construct();
        include('config.php');

        $this->EXAMPLE_GROUP = $example_group;
        $this->EXAMPLE_BAD_GROUP = $example_bad_group;
        $this->EXAMPLE_USER = $example_user;

        $this->grouper = new Grouper($host, $port, $path, $user, $password);
    }

    function testGetGroupMembers() {
        $members = $this->grouper->get_group_members($this->EXAMPLE_GROUP);
        $this->assertContains($this->EXAMPLE_USER, $members);
    }

    function testGetUserGroups() {
        $groups = $this->grouper->get_user_groups($this->EXAMPLE_USER);
        $this->assertContains($this->EXAMPLE_GROUP, $groups);
    }

    function testIsUserAuthorized() {
        $is_authorized = $this->grouper->is_user_authorized($this->EXAMPLE_USER, $this->EXAMPLE_GROUP);
        $this->assertTrue($is_authorized);
    }

    function testIsUserNotAuthorized() {
        $is_authorized = $this->grouper->is_user_authorized($this->EXAMPLE_USER, $this->EXAMPLE_BAD_GROUP);
        $this->assertFalse($is_authorized);
    }

}